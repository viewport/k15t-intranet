
$(document).ready(function(){

    $(".toggleMenu").click(function() {
        $(this).toggleClass("active");
        $(".accordion").toggleClass('showMobileNavigation');
    });

    prettyPrint();

    // Left side panel navigation

    $('.left-side-nav').find('ul').hide();
    $('.current').parents('ul').show().end().children('ul').show();

    /* remove all styles from the macros */
    $( ".inner-content style" ).remove();
    
    /* add a current state to the parents of an open subnav */
    $("li.current" ).parents("li").addClass("current-parent");

    function goToSearch(type, query){
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ?
                ':' + window.location.port : '');
        }

        if(type == "user"){
            return window.location.origin + window.location.pathname + "?q=" + query + "&t=user";
        }else if(type == "page"){
            return window.location.origin + window.location.pathname + "?q=" + query;
        }

    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    if(getCookie('search') == ''){
        document.cookie = "search=page";
    }else if(getCookie('search') == 'user'){
        $('.sidebox .search-form').append('<input type="hidden" name="t" value="user" />');
    }

    $('.search-filters .user').click(function () {
        window.location.replace(goToSearch("user", $('.searchtxt').val()));
        document.cookie = "search=user";
    });

    $('.search-filters .page').click(function () {
        window.location.replace(goToSearch("page", $('.searchtxt').val()));
        document.cookie = "search=page";
    });

})
