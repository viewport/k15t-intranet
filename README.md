Scroll Viewport
===============
K15t Intranet
---------------
![Intranet Thumbnail]()

K15t Intranet Theme, a Viewport theme created specially for intranets.

In order to install this theme 

1. With Scroll Viewport already installed enable FTP through the admin console. 
1. With any FTP client connect to the system and create a new top-level folder - "my-theme" for example.
1. Upload every single file from this folder.
1. Assign a viewport to the space you want under VIEWPORTS in the admin console.
1. Go to the space and under Tools and select "Open Viewport". 

For more information please visit the [Scroll Viewport documentation](http://www.k15t.com/display/VPRT/Documentation). 

Features
--------

* Home / Children Pages 
* User Search


Need Help?
----------

If you have any questions please visit 

* [Viewport Documentation Pages](http://www.k15t.com/display/VPRT/Documentation)
* [Scroll Viewport Developers Group](https://groups.google.com/forum/#!forum/scroll-viewport-dev).

Copyright  
---------

K15t Software GmbH [@k15t](http://twitter.com/k15tsoftware)
Licensed under the Apache License, Version 2.0 

Third party
---------------------------------------------------

jQuery                      Copyright (c) jQuery Foundation.

prettify                    Code at [Google Code](https://code.google.com/p/google-code-prettify/)